# CertBill #

This Perl script is a simple utility to keep tab on cluster costs. It bases itself on the output from the [Maui Cluster Management System](http://www.adaptivecomputing.com/products/open-source/maui/) currently installed on Certainty.

## Summary of set up ##

The code can be run either by
```
$./certBill -u pavang
```

where 'pavang' is your username on Certainty or alternatively, 

```
$./certBill -g
```

in order to view the stats of the entire group.

## Who do I talk to? ##

* Repo owner or admin : [Pavan Bharadwaj](https://bitbucket.org/gpavanb)
* Acknowledgements : Special thanks to Peter Chao Ma and Steve Jones for helping me out with the script
* Other community or team contact : The code was developed at the Flow Physics and Computational Engineering group at Stanford University. Please direct any official queries to [Prof. Matthias Ihme](mailto:mihme@stanford.edu)